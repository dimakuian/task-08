package com.epam.rd.java.basic.task8.model;

public class Price {

    private double value;
    private String currency;

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return "Price{" +
                "price=" + value +
                ", currency='" + currency + '\'' +
                '}';
    }
}
