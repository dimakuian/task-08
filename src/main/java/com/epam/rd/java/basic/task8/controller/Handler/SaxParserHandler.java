package com.epam.rd.java.basic.task8.controller.Handler;


import com.epam.rd.java.basic.task8.model.BreakfastMenu;
import com.epam.rd.java.basic.task8.model.Food;
import com.epam.rd.java.basic.task8.model.Price;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import static com.epam.rd.java.basic.task8.utils.TagsName.*;

public class SaxParserHandler extends DefaultHandler {
    private String currentTagName;
    private Food food;
    private Price price;
    private BreakfastMenu menu;

    public BreakfastMenu getMenu() {
        return menu;
    }

    @Override
    public void startDocument() {
        menu = new BreakfastMenu();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        if (qName.equals(NAME)) {
            food = new Food();
        }
        if (qName.equals(PRICE)) {
            price = new Price();
            String currency = attributes.getValue(CURRENCY);
            price.setCurrency(currency);
        }
        currentTagName = qName;
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        if (qName.equals(FOOD)) {
            menu.setFood(food);
        }
        currentTagName = null;
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        if (currentTagName == null) return;
        if (currentTagName.equals(NAME)) {
            food.setName(new String(ch, start, length));
        }
        if (currentTagName.equals(PRICE)) {
            price.setValue(Double.parseDouble(new String(ch, start, length)));
            food.setPrice(price);
        }
        if (currentTagName.equals(DESCRIPTION)) {
            food.setDescription(new String(ch, start, length));
        }
        if (currentTagName.equals(CALORIES)) {
            food.setCalories(Integer.parseInt(new String(ch, start, length)));
        }
    }
}
