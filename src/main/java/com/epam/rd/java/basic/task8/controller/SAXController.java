package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.controller.Handler.SaxParserHandler;
import com.epam.rd.java.basic.task8.model.BreakfastMenu;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

    private final Logger logger = Logger.getGlobal();
    private final String xmlFileName;

    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public BreakfastMenu parse() {
        BreakfastMenu menu;
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SaxParserHandler saxParserHandler = new SaxParserHandler();
        File file = new File(xmlFileName);
        try {
            SAXParser parser = factory.newSAXParser();
            parser.parse(file, saxParserHandler);
            menu = saxParserHandler.getMenu();
        } catch (ParserConfigurationException e) {
            logger.warning("ParserConfigurationException: " + e.getMessage());
            return null;
        } catch (SAXException e) {
            logger.warning("SAXException: " + e.getMessage());
            return null;
        } catch (IOException e) {
            logger.warning("IOException: " + e.getMessage());
            return null;
        }
        return menu;
    }
}