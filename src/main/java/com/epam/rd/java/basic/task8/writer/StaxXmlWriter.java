package com.epam.rd.java.basic.task8.writer;

import com.epam.rd.java.basic.task8.model.Food;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.logging.Logger;

import static com.epam.rd.java.basic.task8.utils.TagsName.*;



public final class StaxXmlWriter implements XmlWriter {
    private final String outputXML;
    private final Logger logger = Logger.getGlobal();

    public StaxXmlWriter(String outputXML) {
        this.outputXML = outputXML;
    }

    @Override
    public void writeXmlFile(List<Food> foods) {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            XMLOutputFactory factory = XMLOutputFactory.newInstance();
            XMLStreamWriter writer = factory.createXMLStreamWriter(out);

            writeXML(foods, writer);

            String xml = out.toString(StandardCharsets.UTF_8);

            saveXML(xml, outputXML);

        } catch (XMLStreamException | TransformerException | IOException e) {
            logger.warning(e.getMessage());
        }
    }

    private static void saveXML(String xml, String fileName) throws IOException, TransformerException {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();

        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");

        StreamSource source = new StreamSource(new StringReader(xml));
        FileWriter fileWriter = new FileWriter(fileName);
        StreamResult result = new StreamResult(fileWriter);
        transformer.transform(source, result);
    }

    private static void writeXML(List<Food> foods, XMLStreamWriter writer) throws XMLStreamException {
        writer.writeStartDocument();
        writer.writeStartElement(BREAKFAST_MENU);
        writer.writeAttribute("xmlns", "www.bestbreakfast.com.ua");
        writer.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        writer.writeAttribute("xsi:schemaLocation", "www.bestbreakfast.com.ua input.xsd");
        for (Food f : foods) {
            writer.writeStartElement(FOOD);

            writer.writeStartElement(NAME);
            writer.writeCharacters(f.getName());
            writer.writeEndElement();

            writer.writeStartElement(PRICE);
            writer.writeAttribute(CURRENCY, f.getPrice().getCurrency());
            writer.writeCharacters(String.valueOf(f.getPrice().getValue()));
            writer.writeEndElement();

            writer.writeStartElement(DESCRIPTION);
            writer.writeCharacters(f.getDescription());
            writer.writeEndElement();

            writer.writeStartElement(CALORIES);
            writer.writeCharacters(String.valueOf(f.getCalories()));
            writer.writeEndElement();

            writer.writeEndElement();
        }
        writer.writeEndElement();
        writer.writeEndDocument();
        writer.flush();
        writer.close();
    }

}
