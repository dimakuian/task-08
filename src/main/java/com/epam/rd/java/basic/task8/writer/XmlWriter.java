package com.epam.rd.java.basic.task8.writer;

import com.epam.rd.java.basic.task8.model.Food;

import java.util.List;

public interface XmlWriter {
    void writeXmlFile(List<Food> foods);
}
