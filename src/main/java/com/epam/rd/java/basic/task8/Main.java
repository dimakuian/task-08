package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.model.ComparatorImpl.FoodComparatorByCalories;
import com.epam.rd.java.basic.task8.model.ComparatorImpl.FoodComparatorByName;
import com.epam.rd.java.basic.task8.model.ComparatorImpl.FoodComparatorByPrice;
import com.epam.rd.java.basic.task8.model.Food;
import com.epam.rd.java.basic.task8.writer.DomXmlWriter;
import com.epam.rd.java.basic.task8.writer.StaxXmlWriter;
import com.epam.rd.java.basic.task8.writer.XmlWriter;

import java.util.*;

public class Main {

	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}

		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		List<Food> foods1 = domController.parse().getFoods();


		// sort (case 1)
		Collections.sort(foods1,new FoodComparatorByName());
		
		// save
		String outputXmlFile = "output.dom.xml";
		XmlWriter writer = new DomXmlWriter(outputXmlFile);
		writer.writeXmlFile(foods1);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		List<Food> foods2 = saxController.parse().getFoods();

		
		// sort  (case 2)
		Collections.sort(foods2,new FoodComparatorByPrice());
		
		// save
		outputXmlFile = "output.sax.xml";
		writer = new StaxXmlWriter(outputXmlFile);
		writer.writeXmlFile(foods2);

		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		List<Food> foods3 = staxController.parse().getFoods();

		// sort  (case 3)
		Collections.sort(foods3,new FoodComparatorByCalories());
		
		// save
		outputXmlFile = "output.stax.xml";
		writer = new StaxXmlWriter(outputXmlFile);
		writer.writeXmlFile(foods3);
	}

}
