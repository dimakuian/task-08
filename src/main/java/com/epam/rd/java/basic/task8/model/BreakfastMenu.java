package com.epam.rd.java.basic.task8.model;

import java.util.ArrayList;
import java.util.List;

public class BreakfastMenu {
    private final List <Food> foods;

    public BreakfastMenu() {
        foods = new ArrayList<>();
    }

    public List<Food> getFoods() {
        return foods;
    }

    public void setFood(Food food){
        foods.add(food);
    }

    @Override
    public String toString() {
        return "BreakfastMenu{" +
                "foods=" + foods +
                '}';
    }
}
