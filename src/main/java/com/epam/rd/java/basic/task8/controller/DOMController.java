package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.model.BreakfastMenu;
import com.epam.rd.java.basic.task8.model.Food;
import com.epam.rd.java.basic.task8.model.Price;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.logging.Logger;

import static com.epam.rd.java.basic.task8.utils.TagsName.*;


/**
 * Controller for DOM parser.
 */
public class DOMController {

    private final Logger logger = Logger.getGlobal();
    private final String xmlFileName;

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public BreakfastMenu parse (){
        BreakfastMenu breakfastMenu = new BreakfastMenu();
        Document doc = getDocument();
        Node bmNode = null;
        if (doc != null) {
            bmNode = doc.getFirstChild();
        }
        NodeList childNodes = bmNode != null ? bmNode.getChildNodes() : null;
        if (childNodes != null) {
            for (int i = 0; i < childNodes.getLength(); i++) {
                if (childNodes.item(i).getNodeType() != Node.ELEMENT_NODE) continue;

                NodeList foodNodes = childNodes.item(i).getChildNodes();
                Food food = new Food();

                for (int j = 0; j < foodNodes.getLength(); j++) {
                    if (foodNodes.item(j).getNodeType() != Node.ELEMENT_NODE) continue;

                    switch (foodNodes.item(j).getNodeName()){
                        case NAME:
                            food.setName(foodNodes.item(j).getTextContent());
                            break;
                        case PRICE:
                            food.setPrice(getPriceFromNode(foodNodes.item(j)));
                            break;
                        case DESCRIPTION:
                            food.setDescription(foodNodes.item(j).getTextContent());
                            break;
                        case CALORIES:
                            food.setCalories(Integer.parseInt(foodNodes.item(j).getTextContent()));
                            break;
                        default:
                            break;
                    }
                }
                breakfastMenu.setFood(food);
            }
        }
        return breakfastMenu;
    }

    private Price getPriceFromNode(Node node) {
        Price price = new Price();
        double value = Double.parseDouble(node.getTextContent());
        NamedNodeMap attributes = node.getAttributes();
        String currency = attributes.item(0).getTextContent();
        price.setValue(value);
        price.setCurrency(currency);
        return price;
    }

    private Document getDocument() {
        File file = new File(xmlFileName);
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            return dbf.newDocumentBuilder().parse(file);
        } catch (Exception e) {
            logger.warning("Exception" + e.getMessage());
        }
        return null;
    }
}
