package com.epam.rd.java.basic.task8.utils;

import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;


public final class ValidatorXML {
    private ValidatorXML() {
    }

    public static boolean validateXMLSchema(String xsdPath, String xmlPath) {
        Logger logger = Logger.getGlobal();
        try {
            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(new File(xsdPath));
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(xmlPath));
            return true;
        } catch (IOException exception) {
            logger.warning("IOException: " + exception.getMessage());
            return false;
        } catch (SAXException e) {
            logger.warning("SAXException: " + e.getMessage());
            return false;
        }
    }
}
