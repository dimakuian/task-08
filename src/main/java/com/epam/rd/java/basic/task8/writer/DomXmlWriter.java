package com.epam.rd.java.basic.task8.writer;

import com.epam.rd.java.basic.task8.model.Food;
import com.epam.rd.java.basic.task8.writer.XmlWriter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import static com.epam.rd.java.basic.task8.utils.TagsName.*;

public final class DomXmlWriter implements XmlWriter {
    private final String outputXML;

    public DomXmlWriter(String outputXML) {
        this.outputXML = outputXML;
    }

    @Override
    public void writeXmlFile(List<Food> foods) {
        Logger logger = Logger.getGlobal();
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.newDocument();

            writeXML(foods, doc);

            saveXML(outputXML, doc);

        } catch (ParserConfigurationException | TransformerException | IOException e) {
            logger.warning("Exception " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void writeXML(List<Food> foods, Document doc) {
        Element root = doc.createElement(BREAKFAST_MENU);
        root.setAttribute("xmlns", "www.bestbreakfast.com.ua");
        root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        root.setAttribute("xsi:schemaLocation", "www.bestbreakfast.com.ua input.xsd");
        doc.appendChild(root);

        for (Food f : foods) {
            Element food = doc.createElement(FOOD);
            root.appendChild(food);
            Element name = doc.createElement(NAME);
            name.appendChild(doc.createTextNode(f.getName()));
            food.appendChild(name);

            Element price = doc.createElement(PRICE);
            String currency = f.getPrice().getCurrency();
            String value = String.valueOf(f.getPrice().getValue());
            price.setAttribute(CURRENCY, currency);
            price.appendChild(doc.createTextNode(value));
            food.appendChild(price);

            Element description = doc.createElement(DESCRIPTION);
            description.appendChild(doc.createTextNode(f.getDescription()));
            food.appendChild(description);

            Element calories = doc.createElement(CALORIES);
            calories.appendChild(doc.createTextNode(String.valueOf(f.getCalories())));
            food.appendChild(calories);
        }
    }

    private void saveXML(String outputXML, Document doc) throws IOException, TransformerException {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        DOMSource source = new DOMSource(doc);
        FileWriter fileWriter = new FileWriter(outputXML);
        StreamResult result = new StreamResult(fileWriter);
        transformer.transform(source, result);
    }
}
