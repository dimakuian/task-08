package com.epam.rd.java.basic.task8.utils;

public final class TagsName {
    public static final String NAME = "name";
    public static final String BREAKFAST_MENU = "breakfast_menu";
    public static final String FOOD = "food";
    public static final String PRICE = "price";
    public static final String DESCRIPTION = "description";
    public static final String CALORIES = "calories";
    public static final String CURRENCY = "currency";

    private TagsName() {
    }
}
