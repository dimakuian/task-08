package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.model.BreakfastMenu;
import com.epam.rd.java.basic.task8.model.Food;
import com.epam.rd.java.basic.task8.model.Price;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.logging.Logger;

import static com.epam.rd.java.basic.task8.utils.TagsName.*;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {
    private final Logger logger = Logger.getGlobal();
    private final String xmlFileName;


    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public BreakfastMenu parse() {
        BreakfastMenu menu = new BreakfastMenu();
        Food food = new Food();
        Price price = new Price();
        XMLInputFactory factory = XMLInputFactory.newInstance();
        try {
            XMLStreamReader xmlReader = factory.createXMLStreamReader(xmlFileName, new FileInputStream(xmlFileName));
            while (xmlReader.hasNext()) {
                xmlReader.next();

                if (xmlReader.isStartElement()) {
                    switch (xmlReader.getLocalName().toLowerCase()) {
                        case FOOD:
                            food = new Food();
                            break;
                        case NAME:
                            food.setName(xmlReader.getElementText());
                            break;
                        case DESCRIPTION:
                            food.setDescription(xmlReader.getElementText());
                            break;
                        case CALORIES:
                            food.setCalories(Integer.parseInt(xmlReader.getElementText()));
                            break;
                        case PRICE:
                            String currency = xmlReader.getAttributeValue(0);
                            price = new Price();
                            price.setValue(Double.parseDouble(xmlReader.getElementText()));
                            price.setCurrency(currency);
                            break;
                        default:
                            continue;
                    }
                }

                if (xmlReader.isEndElement()) {
                    switch (xmlReader.getLocalName()) {
                        case FOOD:
                            menu.setFood(food);
                            break;
                        case PRICE:
                            food.setPrice(price);
                            break;
                        default:
                    }
                }

            }
        } catch (FileNotFoundException e) {
            logger.warning("FileNotFoundException " + e.getMessage());
            return null;
        } catch (XMLStreamException e) {
            logger.warning("XMLStreamException " + e.getMessage());
            return null;
        }
        return menu;
    }
}